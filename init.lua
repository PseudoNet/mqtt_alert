-- Your access point's SSID and password
local SSID = "xxx"
local SSID_PASSWORD = "xxx"

-- configure ESP as a station
wifi.setmode(wifi.STATION)
wifi.sta.config(SSID, SSID_PASSWORD)
wifi.sta.autoconnect(1)
ip = wifi.sta.getip()
print(ip)

print("Start launch")
--Send the initial message
tmr.alarm(0, 1000, tmr.ALARM_SINGLE, function()
	print("Initial launch")
	dofile("mqtt.lua")
 end)

--Send subsiquest messages every 6 hours
tmr.alarm(1,21600000,1,function() --6 hour Delay 21600000
    print("Timer launch")
    dofile("mqtt.lua")
    end)
