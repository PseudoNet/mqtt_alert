local MQTT_USERNAME = "xxx"
local MQTT_PASSWORD = "xxx"
local MQTT_HOST = "xxx"
local MQTT_PORT = 1883
local MQTT_TOPIC = "emon/traps/rat"
local MQTT_MESSAGE = "triggered"

m = mqtt.Client(wifi.sta.getmac(), 120, MQTT_USERNAME, MQTT_PASSWORD)
m:lwt("/lwt", wifi.sta.getmac(), 0, 0)

tmr.alarm(0, 1000, 1, function()
  if wifi.sta.status() == 5 then
    tmr.stop(0)
    m:connect(MQTT_HOST, MQTT_PORT, 0, function(conn) 
        print("connected")
        m:publish(MQTT_TOPIC, MQTT_MESSAGE, 0, 0, function(conn) 
        	print("sent") 
        	m:close(); 
    	end)
     end)
   end
end)

